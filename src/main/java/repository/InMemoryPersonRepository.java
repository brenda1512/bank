package repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import domain.Bank;
import domain.Person;

public class InMemoryPersonRepository implements PersonRepository {
	
	Map<String, Person> people;
	public  InMemoryPersonRepository() {
		people = new HashMap<String, Person>();
	}
	
	@Override
	public Person findByName(String name) {
		return Person.copy(people.get(name));
	}

	@Override
	public List<Person> findAll() {
		List<Person> list = new ArrayList<Person>();
		for (Person a : people.values()) {
			list.add(Person.copy(a));
		}
		return list;
	}

	@Override
	public Person save(Person person) {
		if (person.getName()== null)  {
			person.setName("Person " + Math.random());
		}
		people.put(person.getName(), person);
		
		return Person.copy(person);
	}

	@Override
	public Person remove(Person person) {
		if (people.containsKey(person.getName()))
			return Person.copy(people.remove(person.getName()));
		return null;
	}

}

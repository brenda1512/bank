package repository;

public class InMemoryRepositoryFactory implements RepositoryFactory {

	@Override
	public AccountRepository createAccountRepository() {
		return new InMemoryAccountRepository();
	}

	@Override
	public PersonRepository createPersonRepository() {
		return new InMemoryPersonRepository();
	}

	@Override
	public BankRepository createBankRepository() {
		return new InMemoryBankRepository();
	}

}

package repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import domain.Bank;

public class InMemoryBankRepository implements BankRepository{

	Map<String, Bank> banks;
	public  InMemoryBankRepository() {
		banks = new HashMap<String, Bank>();
	}
	@Override
	public Bank findByName(String name) {
		return Bank.copy(banks.get(name));
	}

	@Override
	public List<Bank> findAll() {
		List<Bank> list = new ArrayList<Bank>();
		for (Bank a : banks.values()) {
			list.add(Bank.copy(a));
		}
		return list;
	}

	@Override
	public Bank save(Bank bank) {
		if (bank.getName()== null)  {
			bank.setName("Bank " + Math.random());
		}
		banks.put(bank.getName(), bank);
		
		return Bank.copy(bank);
	}

	@Override
	public Bank remove(Bank bank) {
		if (banks.containsKey(bank.getName()))
			return Bank.copy(banks.remove(bank.getName()));
		return null;
	}

}

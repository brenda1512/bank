package repository;

import java.util.List;
import domain.Person;


public interface PersonRepository {
	Person findByName(String name);

	List<Person> findAll();

	Person save(Person person);

	Person remove(Person person);
}

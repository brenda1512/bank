package repository;

import java.util.List;

import domain.Bank;

public interface BankRepository {
	Bank findByName(String name);

	List<Bank> findAll();

	Bank save(Bank bank);

	Bank remove(Bank bank);
}

package domain;

import java.util.List;

public class Bank {
	
	private String name;
	private String address;
	private List<Account> accounts;
	private List<Person> people;
	
	public Bank()
	{
	}
	
	public Bank(String name, String address)
	{
		this.name		=	name;
		this.address	=	address;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public void setName(String name)
	{
		this.name	=	name;
	}
	public void setAddress(String address)
	{
		this.address	=	address;
	}
		
	public static Bank copy(Bank bank) {
		if (bank == null) {
			return null;
		}
		Bank copy = new Bank();
		copy.setName(bank.getName());
		copy.setAddress(bank.getAddress());
		return copy;
	}
}

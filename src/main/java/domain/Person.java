package domain;

public class Person {
	private String name;
	private String address;
	
	public Person()
	{
		
	}
	public Person(String name, String address)
	{
		this.name		=	name;
		this.address	=	address;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public void setName(String name)
	{
		this.name	=	name;
	}
	
	public void setAddress(String address)
	{
		this.address	=	address;
	}
	
	
	public static Person copy(Person person) {
		if (person == null) {
			return null;
		}
		Person copy = new Person();
		copy.setName(person.getName());
		copy.setAddress(person.getAddress());

		return copy;
	}
}

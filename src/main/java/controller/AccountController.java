package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import domain.Account;
import repository.AccountRepository;
import repository.InMemoryAccountRepository;
import service.TransferService;

@Controller
public class AccountController {
	@RequestMapping("/consultar_cuentas")
	String ListaCuentas(ModelMap model) {
		AccountRepository repository = new InMemoryAccountRepository();
		
		Account a1 = new Account("1001", 550);
		Account a2 = new Account("1002", 50);
		repository.save(a1);
		repository.save(a2);
		
		model.addAttribute("Cuentas", repository.findAll());
		
		return "AccountsList";
	}
}

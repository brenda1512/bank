package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import domain.Account;
import domain.Bank;
import repository.AccountRepository;
import repository.BankRepository;
import repository.InMemoryAccountRepository;
import repository.InMemoryBankRepository;
import service.TransferService;

@Controller

public class BankController {
	@RequestMapping("/consultar_bancos")
	String ListaCuentas(ModelMap model) {
		BankRepository repository = new InMemoryBankRepository();
		
		Bank b1 = new Bank("BCP", "Av. Ejercito");
		Bank b2 = new Bank("BBVA", "Mall Aventura");
		repository.save(b1);
		repository.save(b2);
		
		model.addAttribute("Bancos", repository.findAll());
		
		return "BanksList";
	}
}

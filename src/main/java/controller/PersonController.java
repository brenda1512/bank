package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import domain.Account;
import domain.Bank;
import domain.Person;
import repository.AccountRepository;
import repository.BankRepository;
import repository.InMemoryAccountRepository;
import repository.InMemoryBankRepository;
import repository.InMemoryPersonRepository;
import repository.PersonRepository;
import service.TransferService;

@Controller
public class PersonController {
	@RequestMapping("/consultar_personas")
	String ListaCuentas(ModelMap model) {
		PersonRepository repository = new InMemoryPersonRepository();
		
		Person p1 = new Person("Juan Perez Lopez", "Av. Avacion 320");
		Person p2 = new Person("Sofia Delgado Pinto", "Calle San Agustin 123");
		repository.save(p1);
		repository.save(p2);
		
		model.addAttribute("Personas", repository.findAll());
		
		return "PeopleList";
	}
}

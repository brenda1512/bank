package controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import domain.Account;
import repository.AccountRepository;
import repository.InMemoryAccountRepository;
import service.TransferService;

@Controller

public class TransferController {
    @RequestMapping(value="/transfer")
    public String TransferForm() {

        return "Transfer";
    }

    @RequestMapping(value="/operation")
    public String transferSubmit(ModelMap model, @RequestParam String sourceNumber,@RequestParam String targetNumber, @RequestParam double amount)
    {
    	AccountRepository repository = new InMemoryAccountRepository();
		
		Account a1 = new Account("1001", 550);
		Account a2 = new Account("1002", 50);
		repository.save(a1);
		repository.save(a2);
		
    	TransferService transfer = new TransferService(repository);
    	model.addAttribute("result",transfer.transfer(sourceNumber, targetNumber, amount));
    	
    	return "TransferResult";   	
    	
    }
}
